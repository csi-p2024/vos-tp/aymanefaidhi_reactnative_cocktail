import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './Composantes/HomeScreen';
import DetailsScreen from './Composantes/DetailsScreen';
import FavoritesScreen from './Composantes/FavoritesScreen';
import SearchScreen from './Composantes/SearchScreen';
import IngredientSearchScreen from './Composantes/IngredientSearchScreen'; 

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Cocktails' }} />
        <Stack.Screen name="Details" component={DetailsScreen} options={{ title: 'Information du Cocktail' }} />
        <Stack.Screen name="Favorites" component={FavoritesScreen} options={{ title: 'Cocktail(s) favoris' }} />
        <Stack.Screen name="Search" component={SearchScreen} options={{ title: 'Rechercher un cocktail' }} />
        <Stack.Screen name="IngredientSearch" component={IngredientSearchScreen} options={{ title: 'Recherche par ingrédient' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
