import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, Button } from 'react-native';

const DetailsScreen = ({ route, navigation }) => {
  const { cocktail, favorites, setFavorites } = route.params;

  const addToFavorites = () => {
    if (!favorites.find(fav => fav.idDrink === cocktail.idDrink)) {
      setFavorites([...favorites, cocktail]);
    }
  };

  const renderIngredients = () => {
    let ingredients = [];
    for (let i = 1; i <= 15; i++) {
      if (cocktail[`strIngredient${i}`]) {
        ingredients.push(
          <Text key={i} style={styles.ingredient}>
            {cocktail[`strIngredient${i}`]} - {cocktail[`strMeasure${i}`]}
          </Text>
        );
      }
    }
    return ingredients;
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Image source={{ uri: cocktail.strDrinkThumb }} style={styles.image} />
      <Text style={styles.name}>{cocktail.strDrink}</Text>
      <Text style={styles.category}>{cocktail.strCategory}</Text>
      <Text style={styles.instructionsTitle}>Instructions</Text>
      <Text style={styles.instructions}>{cocktail.strInstructions}</Text>
      <Text style={styles.ingredientsTitle}>Ingredients</Text>
      {renderIngredients()}
      <Button title="Add to Favorites" onPress={addToFavorites} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  image: {
    width: 200,
    height: 200,
    borderRadius: 100,
  },
  name: {
    marginTop: 20,
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  category: {
    fontSize: 18,
    color: 'gray',
    marginVertical: 10,
  },
  instructionsTitle: {
    marginTop: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
  instructions: {
    marginTop: 10,
    fontSize: 16,
    textAlign: 'center',
  },
  ingredientsTitle: {
    marginTop: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
  ingredient: {
    marginTop: 10,
    fontSize: 16,
  },
});

export default DetailsScreen;
