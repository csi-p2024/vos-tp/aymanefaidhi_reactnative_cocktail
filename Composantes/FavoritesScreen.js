import React, { useState } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image } from 'react-native';

const FavoritesScreen = ({ navigation, route }) => {
  const [favorites, setFavorites] = useState(route.params?.favorites || []);

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => navigation.navigate('Details', { cocktail: item })}>
      <View style={styles.container}>
        <Image source={{ uri: item.strDrinkThumb }} style={styles.image} />
        <Text style={styles.name}>{item.strDrink}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <FlatList
      data={favorites}
      renderItem={renderItem}
      keyExtractor={item => item.idDrink}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    margin: 10,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  name: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FavoritesScreen;
