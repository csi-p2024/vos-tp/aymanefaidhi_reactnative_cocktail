import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image, Button } from 'react-native';

const HomeScreen = ({ navigation }) => {
  const [cocktails, setCocktails] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [favorites, setFavorites] = useState([]);

  const fetchCocktails = async () => {
    if (loading) return;
    setLoading(true);
    const response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?f=${String.fromCharCode(96 + page)}`);
    const data = await response.json();
    setCocktails([...cocktails, ...data.drinks]);
    setPage(page + 1);
    setLoading(false);
  };

  useEffect(() => {
    fetchCocktails();
  }, []);

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => navigation.navigate('Details', { cocktail: item, favorites, setFavorites })}>
      <View style={styles.container}>
        <Image source={{ uri: item.strDrinkThumb }} style={styles.image} />
        <Text style={styles.name}>{item.strDrink}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.buttonContainer}>
        <View style={styles.button}>
          <Button title="Les favoris" onPress={() => navigation.navigate('Favorites', { favorites, setFavorites })} />
        </View>
        <View style={styles.button}>
          <Button title="Rechercher un cocktail" onPress={() => navigation.navigate('Search', { favorites, setFavorites })} />
        </View>
        <View style={styles.button}>
          <Button title="Rechercher par ingrédient" onPress={() => navigation.navigate('IngredientSearch', { favorites, setFavorites })} />
        </View>
      </View>
      <FlatList
        data={cocktails}
        renderItem={renderItem}
        keyExtractor={item => item.idDrink}
        onEndReached={fetchCocktails}
        onEndReachedThreshold={0.5}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    margin: 10,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  name: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  button: {
    marginVertical: 10,
    width: '80%', 
  },
});

export default HomeScreen;
