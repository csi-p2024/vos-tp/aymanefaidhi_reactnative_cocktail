import React, { useState } from 'react';
import { View, TextInput, FlatList, StyleSheet, TouchableOpacity, Image, Text } from 'react-native';

const IngredientSearchScreen = ({ navigation, route }) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [cocktails, setCocktails] = useState([]);
  const { favorites, setFavorites } = route.params;

  const searchCocktails = async () => {
    if (!searchQuery) return;
    const response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${searchQuery}`);
    const data = await response.json();
    setCocktails(data.drinks);
  };

  const fetchCocktailDetails = async (id) => {
    const response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`);
    const data = await response.json();
    return data.drinks[0];
  };

  const handlePress = async (id) => {
    const cocktail = await fetchCocktailDetails(id);
    navigation.navigate('Details', { cocktail, favorites, setFavorites });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => handlePress(item.idDrink)}>
      <View style={styles.container}>
        <Image source={{ uri: item.strDrinkThumb }} style={styles.image} />
        <Text style={styles.name}>{item.strDrink}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.screenContainer}>
      <TextInput
        style={styles.input}
        placeholder="Search by ingredient..."
        value={searchQuery}
        onChangeText={setSearchQuery}
        onSubmitEditing={searchCocktails}
      />
      <FlatList
        data={cocktails}
        renderItem={renderItem}
        keyExtractor={item => item.idDrink}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    padding: 10,
    backgroundColor: '#F5FCFF',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    margin: 10,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  name: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default IngredientSearchScreen;
